/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Users;

import entities.Users;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 *
 * @author Tariana
 */
public class Login extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String pwd = request.getParameter("pwd");
        
        SessionFactory sessionFactory;
        ServiceRegistry serviceRegistry;

        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Users.class)
            .configure();
        
        serviceRegistry = new ServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .configure("hibernate.cfg.xml")
                .build();        
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        HttpSession session = request.getSession();
        Session sess = sessionFactory.openSession();
        String hql = "FROM Users";
        Query queryy = sess.createQuery(hql);
        List<Users> buying;
        List results = queryy.list();
        
        boolean logsuccess = false;
        boolean useravail = false;
        buying = results;
        Iterator<Users> itr = buying.iterator();
        while(itr.hasNext()){
            Users g = itr.next();
            if((email.equals(g.getEmail())) && (pwd.equals(g.getPassword()))){
                logsuccess = true;
                session.setAttribute("uname", g.getUsername());
                System.out.println(email + g.getEmail());
                System.out.println(pwd + g.getPassword());
            }
            if(email.equals(g.getEmail())){
                useravail = true;
            }
        }
        if(useravail == false){
            try (PrintWriter out = response.getWriter()) {
                out.println("<script type=\"text/javascript\">");
                out.println("self.location = \"/Eventer/Users/Login.jsp\";");
                out.println("alert('User is not available!!');");
                out.println("</script>");
            }
        }else if((logsuccess == true) && (useravail == true)){
            
            session.setAttribute("mail", email);
            response.sendRedirect("/Eventer/Choice.jsp");
        }
        else{
            try (PrintWriter out = response.getWriter()) {
                out.println("<script type=\"text/javascript\">");
                out.println("self.location.href = \"/Eventer/Users/Login.jsp\";");
                out.println("alert('Incorrect login details!!');");
                out.println("</script>");
            }
        }
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
