/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Users;

import com.mchange.v2.io.FileUtils;
import entities.Users;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.codec.binary.Base64;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 *
 * @author Tariana
 */
public class Register extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Users user = new Users();
        
        String image = request.getParameter("image");
        String email = request.getParameter("email");
        String username = request.getParameter("username");
        String pwd = request.getParameter("pwd");
        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String phoneno = request.getParameter("phoneno");
        String Sex = request.getParameter("Sex");
        int phone = Integer.parseInt(phoneno);
        
        File file = new File(image);
        
        byte[] bFile = new byte[(int) file.length()];
        
        try {
            //convert file into array of bytes
            try (FileInputStream fileInputStream = new FileInputStream(file)) {
                //convert file into array of bytes
                fileInputStream.read(bFile);
            }
        } catch (IOException e) {
	     e.printStackTrace();
        }
        
        //encodes the image bytes into an encodebase64
        byte[] encodeBase64 = Base64.encodeBase64(bFile);
        //converts the encodebase64 to a string which helps display the image
        String base64DataString = new String(encodeBase64 , "UTF-8");
        
        user.setEmail(email);
        user.setFirstname(fname);
        user.setGender(Sex);
        user.setImage(base64DataString);
        user.setLastname(lname);
        user.setPassword(pwd);
        user.setPhoneno(phone);
        user.setUsername(username);
        
        SessionFactory sessionFactory;
        ServiceRegistry serviceRegistry;

        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Users.class)
            .configure();
        
        serviceRegistry = new ServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .configure("hibernate.cfg.xml")
                .build();        
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        
        Session sess = sessionFactory.openSession();
        String hql = "FROM Users";
        Query queryy = sess.createQuery(hql);
        List<Users> buying;
        List results = queryy.list();
        
        buying = results;
        Iterator<Users> itr = buying.iterator();    
        while(itr.hasNext()){
            Users g = itr.next();
            System.out.println(user.getPhoneno() + g.getPhoneno());
            System.out.println(user.getEmail() + g.getEmail());
            
            if(g.getEmail().equals(user.getEmail())){
                try (PrintWriter out = response.getWriter()) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("window.location.href = \"/Eventer/Users/SignUp.jsp\";");
                    out.println("alert('Email already exists!!');");
                    out.println("</script>");
                }
                System.out.println(user.getEmail() + g.getEmail());
            }
            else if(user.getUsername().equalsIgnoreCase(g.getUsername())){
                try (PrintWriter out = response.getWriter()) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("window.location.href = \"/Eventer/Users/SignUp.jsp\";");
                    out.println("alert('Username already exists!!');");
                    out.println("</script>");
                }
                System.out.println(user.getUsername() + g.getUsername());
            }
            else if(user.getPhoneno() == g.getPhoneno()){
                try (PrintWriter out = response.getWriter()) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("window.location.href = \"/Eventer/Users/SignUp.jsp\";");
                    out.println("alert('Phone number already exists!!');");
                    out.println("</script>");
                }
                System.out.println(user.getPhoneno() + g.getPhoneno());
            }else {
                System.out.println("No available username, password and email");
            }
        }
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<h3>Loading...</h3>");
            out.println("<h3>If image is not displayed here you may need to change it</h3>");
            out.println("<img src=\"data:image/png;base64,"); 
            out.println(base64DataString);
            out.println(" height=\"300px\" width=\"300px\">");
            out.println("<img src=\"data:image/png;base64,"); 
            out.println(user.getImage());
            out.println(" height=\"300px\" width=\"300px\">");
            out.println("<script type=\"text/javascript\">");
            out.println("window.location.href = \"/Eventer/Users/Login.jsp\";");
            out.println("alert('Successful Registration!!');");
            out.println("</script>");
            out.println("</html>");
        }
        sess.beginTransaction();
        sess.persist(user);
        sess.getTransaction().commit();
        sess.close();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
