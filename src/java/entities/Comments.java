package entities;
// Generated Jul 23, 2019 11:11:55 AM by Hibernate Tools 4.3.1



/**
 * Comments generated by hbm2java
 */
public class Comments  implements java.io.Serializable {


     private Integer id;
     private Users user;
     private Event event;
     private Users owner;
     private String comment;

    public Comments() {
    }

	
    public Comments(String comment) {
        this.comment = comment;
    }
    public Comments(Users user, Event event, Users owner, String comment) {
       this.user = user;
       this.event = event;
       this.owner = owner;
       this.comment = comment;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public Users getUser() {
        return this.user;
    }
    
    public void setUser(Users user) {
        this.user = user;
    }
    public Event getEvent() {
        return this.event;
    }
    
    public void setEvent(Event event) {
        this.event = event;
    }
    public Users getOwner() {
        return this.owner;
    }
    
    public void setOwner(Users owner) {
        this.owner = owner;
    }
    public String getComment() {
        return this.comment;
    }
    
    public void setComment(String comment) {
        this.comment = comment;
    }




}


