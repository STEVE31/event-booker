/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Redirector;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Tariana
 */
public class Redirect extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String redirect = request.getParameter("redirect");
        String venueid = request.getParameter("eventid");
        HttpSession session = request.getSession(false);
        session.setAttribute("venueid", venueid);
        
        if(redirect.equalsIgnoreCase("bookvenue")){
            System.out.println("bookvenue");
            try (PrintWriter out = response.getWriter()) {
                out.println("<script type=\"text/javascript\">");
                out.println("self.location = \"/Eventer/Book/Book.jsp\";");
                out.println("</script>");
            }
        }else if(redirect.equalsIgnoreCase("deletevenue")){
            System.out.println("deletevenue");
            try (PrintWriter out = response.getWriter()) {
                out.println("<script type=\"text/javascript\">");
                out.println("self.location = \"/Eventer/Delete\";");
                out.println("</script>");
            }
        }else if(redirect.equalsIgnoreCase("editvenue")){
            System.out.println("editvenue");
            try (PrintWriter out = response.getWriter()) {
                out.println("<script type=\"text/javascript\">");
                out.println("self.location = \"/Eventer/Advert/EditVenue.jsp\";");
                out.println("</script>");
            }
        }else if(redirect.equalsIgnoreCase("userviewvenue")){
            System.out.println("userviewvenue");
            try (PrintWriter out = response.getWriter()) {
                out.println("<script type=\"text/javascript\">");
                out.println("self.location = \"/Eventer/Advert/View.jsp\";");
                out.println("</script>");
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
