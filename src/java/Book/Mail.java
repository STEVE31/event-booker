/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editorr.
 */
package Book;

import entities.Event;
import entities.Users;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.PreencodedMimeBodyPart;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Tariana
 */
public class Mail extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if(session.getAttribute("mail") == null){
            try (PrintWriter out = response.getWriter()) {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('You need to login first!!');");
                out.println("window.location.href = \"/Eventer/Users/Login.jsp\";");
                out.println("</script>");
            }
        }else{
            Users owner = (Users) session.getAttribute("owner");
            String capacity = (String) session.getAttribute("capacity");
            Event venue = (Event) session.getAttribute("venue");
            String eventname = (String) session.getAttribute("eventname");
            String date = (String) session.getAttribute("date");
            String mail = new String();
            mail = (String) session.getAttribute("mail");
            Users booker = (Users) session.getAttribute("booker");
            String host = "smtp-mail.outlook.com";
            String user = "njorogesteve30@outlook.com";
            String password = "steniverse30";
            String to = mail;
            Properties props = new Properties();
            props.put("mail.transport.protocol", "smtp" );
            props.put("mail.smtp.starttls.enable","true" );
            props.put("mail.smtp.host",host);  
            props.put("mail.smtp.auth", "true");
            Session sessionn = Session.getDefaultInstance(props,  
                new javax.mail.Authenticator() {  
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {  
                        return new PasswordAuthentication(user,password);  
                    }  
            });
            MimeMessage message;
            try {
                message = new MimeMessage(sessionn);
                message.setFrom(new InternetAddress(user));  
                message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));  
                message.setSubject("Event Booker");  
                String msgText1 = "You have booked an event  \r\n";
                String msgText2 = "Here are the details of the event \r\n";
                Multipart mp = new MimeMultipart();
                MimeBodyPart mbp1 = new MimeBodyPart();
                byte[] imgBytes = Base64.getDecoder().decode(venue.getImage());
                mbp1.setText(msgText1 + "\r\n" 
                        + msgText2 + "\r\n" 
                        + venue.getName() 
                        + " will be hosting a " + eventname 
                        + "\r\n" + "On: " + date 
                        + "\r\n" + "For a capacity of: " + capacity
                        + "\r\n" + "Courtesy of: " + owner.getUsername()
                        + "\r\n" + venue.getDescription()
                        + "\r\n");
                MimeBodyPart filePart = new PreencodedMimeBodyPart("base64");
                filePart.setContent(venue.getImage(), "image/*");
                filePart.setFileName("venueimage.png");
                mp.addBodyPart(mbp1);
                mp.addBodyPart(filePart);
                message.setContent(mp);
                Transport.send(message);
                System.out.println(to);
                System.out.println("message sent successfully...");
            } catch (MessagingException ex) {
                Logger.getLogger(newpackage.Booking.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println(ex);
            }
            try (PrintWriter out = response.getWriter()) {
                out.println("<script type=\"text/javascript\">");
                out.println("window.location.href = \"/Eventer/MailOwner\";");
                out.println("</script>");
            }
        }
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
