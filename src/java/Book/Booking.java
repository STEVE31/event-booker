/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Book;

import entities.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 *
 * @author Tariana
 */
public class Booking extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if(session.getAttribute("mail") == null){
            try (PrintWriter out = response.getWriter()) {
            out.println("<script type=\"text/javascript\">");
            out.println("alert('You need to login first!!');");
            out.println("window.location.href = \"/Eventer/Users/Login.jsp\";");
            out.println("</script>");
            }
        }else{
            String mail = (String) session.getAttribute("mail");
            System.out.println(mail);
            System.out.println(mail);
            System.out.println(mail);
            System.out.println(mail);
            System.out.println(mail);
            System.out.println(mail);
            Event venue = (Event) session.getAttribute("venue");
            Users owner = (Users) session.getAttribute("owner");
            String eventname = request.getParameter("eventname");
            session.setAttribute("eventname", eventname);
            Users user = new Users();
            newpackage.Booking venting = new newpackage.Booking();
            String date = request.getParameter("date");
            session.setAttribute("date", date);
            String capacity = request.getParameter("capacity");
            session.setAttribute("capacity", capacity);
            System.out.println(date);
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
            sdf.setLenient(true);
            try {
                sdf.parse(date.trim());
                System.out.println("date");
            } catch (ParseException pe) {
                System.out.println(pe);
            }
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            System.out.println(sdf.format(timestamp));
            String stamp = sdf.format(timestamp);
            Date pickeddate = new Date();
            Date currentdate = new Date();
            try {
                pickeddate = sdf.parse(date.trim());
                currentdate = sdf.parse(stamp.trim());
            } catch (ParseException ex) {
                System.out.println(ex);
            }
            boolean active = compareDates(pickeddate, currentdate);
            if(active == true){
                 SessionFactory sessionFactory;
            ServiceRegistry serviceRegistry;

            Configuration configuration = new Configuration();
            configuration.addAnnotatedClass(Users.class)
                .configure();

            serviceRegistry = new ServiceRegistryBuilder()
                    .applySettings(configuration.getProperties())
                    .configure("hibernate.cfg.xml")
                    .build();        
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            Session sess = sessionFactory.openSession();
            String hql = "FROM Users";
            Query queryy = sess.createQuery(hql);
            List<Users> buying;
            List results = queryy.list();

            buying = results;
            Iterator<Users> itr = buying.iterator();
            while(itr.hasNext()){
                Users g = itr.next();
                if(g.getEmail().equalsIgnoreCase(mail)){
                    user = g;
                    System.out.println("howdy har har");
                    venting.setDate(date);
                    venting.setEventname(eventname);
                    venting.setOwner(owner.toString());
                    venting.setUser(user.toString());
                    session.setAttribute("booker", user);
                    venting.setVenue(venue.toString());
                    venting.setCapacity(capacity);
                    venting.setEmail(mail);
                    System.out.println(capacity);
                    if(capacity.equalsIgnoreCase("upto100")){
                        venting.setPricing(venue.getUpto100());
                    }else if(capacity.equalsIgnoreCase("upto300")){
                        venting.setPricing(venue.getUpto300());
                    }else if(capacity.equalsIgnoreCase("upto500")){
                        venting.setPricing(venue.getUpto500());
                    }else if(capacity.equalsIgnoreCase("upto1000")){
                        venting.setPricing(venue.getUpto1000());
                    }
                    sess.beginTransaction();
                    sess.persist(venting);
                    sess.getTransaction().commit();
                    sess.close();
                    try (PrintWriter out = response.getWriter()) {
                        out.println("<script type=\"text/javascript\">");
                        out.println("window.location.href = \"/Eventer/Mail\";");
                        out.println("</script>");
                    }
                }
            }
            
                
            }else{
                try (PrintWriter out = response.getWriter()) {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('You need to pick a valid date!!');");
                out.println("window.location.href = \"/Eventer/Book/Book.jsp\";");
                out.println("</script>");
                }
            }
        }
    }
    
    public static boolean compareDates(Date date1,Date date2)
    {
        if(date1.after(date2)){
            System.out.println("Date1 is after Date2");
            return true;
        }
        
        if(date1.before(date2)){
            System.out.println("Date1 is before Date2");
            return false;
        }

        if(date1.equals(date2)){
            System.out.println("Date1 is equal Date2");
            return true;
        }
        return false;
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
