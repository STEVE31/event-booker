/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Advertise;

import entities.Event;
import entities.Users;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 *
 * @author Tariana
 */
public class Delete extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        String eventid = (String) session.getAttribute("venueid");
        int id = Integer.parseInt(eventid);
        
        SessionFactory sessionFactory;
        ServiceRegistry serviceRegistry;

        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Users.class)
            .configure();

        serviceRegistry = new ServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .configure("hibernate.cfg.xml")
                .build();        
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        Session sess = sessionFactory.openSession();
        String hql = "SELECT e FROM Event e WHERE e.id = :id";
        Query query = sess.createQuery(hql);
        query.setInteger("id", id);
        List<Event> products;
        List results = query.list();
        products = results;

        Users own = null;
        Event venues = null;
        String upto;

        Iterator<Event> itr = products.iterator();
        while(itr.hasNext()){
            Event g = itr.next();
            System.out.println(g.getUpto100());
            venues = g;
        }
        venues.setOwner(null);
        sess.beginTransaction();
        sess.delete(venues);
        sess.getTransaction().commit();
        try (PrintWriter out = response.getWriter()) {
            out.println("<script type=\"text/javascript\">");
            out.println("self.location = \"/Eventer/Advert/MyAdverts.jsp\";");
            out.println("alert('Event Venue has been deleted!!');");
            out.println("</script>");
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
