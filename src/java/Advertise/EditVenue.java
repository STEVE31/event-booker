/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Advertise;

import entities.Event;
import entities.Users;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.tomcat.util.codec.binary.Base64;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 *
 * @author Tariana
 */
public class EditVenue extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        String mail;
        if(session.getAttribute("mail") == null || session.getAttribute("mail") == ""){
            try (PrintWriter out = response.getWriter()) {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('You need to login first!!');");
                out.println("window.location.href = \"/Eventer/Users/Login.jsp\";");
                out.println("</script>");
            }
        }else{
            
            mail = (String) session.getAttribute("mail");
            System.out.println(mail);
            Event venues = (Event) session.getAttribute("venue");
            Users owner = new Users();

            String image = request.getParameter("image");
            String email = request.getParameter("email");
            String venue = request.getParameter("venue");
            String upto = request.getParameter("upto");
            System.out.println(request.getParameter("upto"));
            String upto100 = request.getParameter("upto100");
            String upto300 = request.getParameter("upto300");
            String upto500 = request.getParameter("upto500");
            String upto1000 = request.getParameter("upto1000");
            String phoneno = request.getParameter("phoneno");
            String location = request.getParameter("location");
            String Scenario = request.getParameter("Scenario");
            String description = request.getParameter("description");
            int phone = Integer.parseInt(phoneno);
            System.out.println(image);
            System.out.println(upto);
            System.out.println(Scenario);
            System.out.println(upto1000);
            
            if(!image.isEmpty()){
                File file = new File(image);
                byte[] bFile = new byte[(int) file.length()];

                try {
                    //convert file into array of bytes
                    try (FileInputStream fileInputStream = new FileInputStream(file)) {
                        //convert file into array of bytes
                        fileInputStream.read(bFile);
                    }
                } catch (IOException e) {
                     e.printStackTrace();
                }

                //encodes the image bytes into an encodebase64
                byte[] encodeBase64 = Base64.encodeBase64(bFile);
                //converts the encodebase64 to a string which helps display the image
                String base64DataString = new String(encodeBase64 , "UTF-8");

                venues.setImage(base64DataString);
            }
            
            SessionFactory sessionFactory;
            ServiceRegistry serviceRegistry;

            Configuration configuration = new Configuration();
            configuration.addAnnotatedClass(Users.class)
                .configure();

            serviceRegistry = new ServiceRegistryBuilder()
                    .applySettings(configuration.getProperties())
                    .configure("hibernate.cfg.xml")
                    .build();        
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);

            Session sess = sessionFactory.openSession();
            String hql = "FROM Users";
            Query queryy = sess.createQuery(hql);
            List<Users> buying;
            List results = queryy.list();
            buying = results;
            Iterator<Users> itr = buying.iterator();
            while(itr.hasNext()){
                Users g = itr.next();
                if(mail.equals(g.getEmail())){
                    owner = g;
                }
            }
            
            venues.setLocation(location);
            venues.setMail(email);
            venues.setName(venue);
            venues.setOwner(owner);
            venues.setPhoneno(phone);
            venues.setScenario(Scenario);
            venues.setDescription(description);
            venues.setUpto100(Integer.parseInt(upto100));
            if(upto.equalsIgnoreCase("upto1000")){
                venues.setUpto300(Integer.parseInt(upto300));
                venues.setUpto500(Integer.parseInt(upto500));
                venues.setUpto1000(Integer.parseInt(upto1000));
                System.out.println(upto);
            }else if(upto.equalsIgnoreCase("upto500")){
                venues.setUpto300(Integer.parseInt(upto300));
                venues.setUpto500(Integer.parseInt(upto500));
                venues.setUpto1000(null);
                System.out.println(upto);
            }else if(upto.equalsIgnoreCase("upto300")){
                venues.setUpto300(Integer.parseInt(upto300));
                System.out.println(upto);
                venues.setUpto500(null);
                venues.setUpto1000(null);
            }else if(upto.equalsIgnoreCase("upto100")){
                venues.setUpto300(null);
                venues.setUpto500(null);
                venues.setUpto1000(null);
            }
            
            
            sess.beginTransaction();
            sess.update(venues);
            sess.getTransaction().commit();
            sess.close();
            try (PrintWriter out = response.getWriter()) {
                out.println("<script type=\"text/javascript\">");
                out.println("self.location = \"/Eventer/Advert/MyAdverts.jsp\";");
                out.println("alert('Event Venue has been updated!!');");
                out.println("</script>");
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
