<%-- 
    Document   : PickVenue
    Created on : Jul 19, 2019, 10:28:08 PM
    Author     : Tariana
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.service.ServiceRegistryBuilder"%>
<%@page import="entities.*"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="org.hibernate.service.ServiceRegistry"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    SessionFactory sessionFactory;
    ServiceRegistry serviceRegistry;

    Configuration configuration = new Configuration();
    configuration.addAnnotatedClass(Users.class)
        .configure();

    serviceRegistry = new ServiceRegistryBuilder()
            .applySettings(configuration.getProperties())
            .configure("hibernate.cfg.xml")
            .build();        
    sessionFactory = configuration.buildSessionFactory(serviceRegistry);

    Session sess = sessionFactory.openSession();
    String hql = "FROM Event";
    Query queryy = sess.createQuery(hql);
    List<Event> buying;
    List results = queryy.list();
    buying = results;
    Iterator<Event> itr = buying.iterator();
    Users owner;
    %>
<html>
    <head>
        <%@include file = "BookHeader.jsp" %>
        <title>Pick Venue</title>
    </head>
    <body class="eventer">
        <div class="kadi">
            <%
                while(itr.hasNext()){
                Event g = itr.next();
                System.out.println(g.getUpto100());
                owner = g.getOwner();
                /*String u = g.toStreng(g);
                System.out.println(u);
                Event b = g.fromStreng(u);
                System.out.println(u);
                System.out.println(b.toStreng(b));*/
                %>
            <div class="card">
                <img class="card-img-top" src="data:image/png;base64, <%=g.getImage()%>" onerror="this.onerror=null;this.src='event.png';"  alt="">
                <div class="card-body">
                    <h4 class="card-title"><%=g.getName()%></h4>
                    <p class="card-text"><%=g.getLocation()%></p>
                    <p class="card-text eve">Owned by: </p><b><%=owner.getUsername()%></b>
                    <form action="/Eventer/Redirect">
                        <div class="form-group">
                            <label class="sr-only" for="eventview">Hidden input label</label>
                            <input type="text" class="form-control" name="redirect" id="inputName" placeholder="" value="bookvenue" hidden>
                            <input type="text" class="form-control" name="eventid" id="inputName" placeholder="" value="<%=g.getId()%>" hidden>
                            <button type="submit" class="btn-view">Book Venue</button>
                        </div>
                    </form>
                </div>
            </div>
            <%
                }
                sess.close();
                %>
        </div>
    </body>
</html>
