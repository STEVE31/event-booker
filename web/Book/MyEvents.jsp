<%-- 
    Document   : MyEvents
    Created on : Jul 23, 2019, 11:17:46 AM
    Author     : Tariana
--%>

<%@page import="entities.Event"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="newpackage.Booking"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.service.ServiceRegistryBuilder"%>
<%@page import="entities.Users"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="org.hibernate.service.ServiceRegistry"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    //checks whether there is a farmer account with a session within the system
    //this header spans over all the pages after the farmer has logged into the system
    if(session.getAttribute("mail") == null){
            out.println("<script type=\"text/javascript\">");
            out.println("alert('You need to login first!!');");
            out.println("window.location.href = \"/Eventer/Users/Login.jsp\";");
            out.println("</script>");
    }else{
        %>
        <html>
            <head>
                <%@include file="BookHeader.jsp" %>
                <title>My Events</title>
            </head>
            <body class="eventer">
                <div class="kadi">
            <%
                SessionFactory sessionFactory;
                ServiceRegistry serviceRegistry;

                Configuration configuration = new Configuration();
                configuration.addAnnotatedClass(Users.class)
                    .configure();

                serviceRegistry = new ServiceRegistryBuilder()
                        .applySettings(configuration.getProperties())
                        .configure("hibernate.cfg.xml")
                        .build();        
                sessionFactory = configuration.buildSessionFactory(serviceRegistry);

                Session sess = sessionFactory.openSession();
                String hql = "FROM Booking";
                Query queryy = sess.createQuery(hql);
                List<Booking> buying;
                List results = queryy.list();
                buying = results;
                Iterator<Booking> itr = buying.iterator();
                Users owner;
                Event venue;
                Booking event = new Booking();
                System.out.println();
                int i = 0;
                while(itr.hasNext()){
                    Booking g = itr.next();
                    event = g;
                    i++;
                    if(g.getEmail() != null && g.getEmail().equalsIgnoreCase((String) session.getAttribute("mail"))){
                        %>
                        <div class="card">
                            <img class="card-img-top" src="event.png" alt="">
                            <div class="card-body">
                                <h4 class="card-title"><%=event.getEventname()%></h4>
                                <p class="card-text">Date: <%=event.getDate()%></p>
                                <p class="card-text">Capacity for: <%=event.getCapacity()%></p>
                                <p class="card-text eve">ksh.<%=event.getPricing()%></p>
                            </div>
                        </div>
                    <%
                    }
                    if(i == 0){
                    %>
                    <h1>You have not created any event yet</h1>
                    <%
                        }
                    }
                    %>
                </div>
            </body>
        </html>
        <%
    }
%>
