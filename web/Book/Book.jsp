<%-- 
    Document   : Book
    Created on : Jul 21, 2019, 8:27:27 PM
    Author     : Tariana
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.service.ServiceRegistry"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="org.hibernate.service.ServiceRegistryBuilder"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page import="entities.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String eventid = (String) session.getAttribute("venueid");
    Users owner = new Users();
    int id = Integer.parseInt(eventid);
    
    SessionFactory sessionFactory;
    ServiceRegistry serviceRegistry;

    Configuration configuration = new Configuration();
    configuration.addAnnotatedClass(Users.class)
        .configure();

    serviceRegistry = new ServiceRegistryBuilder()
            .applySettings(configuration.getProperties())
            .configure("hibernate.cfg.xml")
            .build();        
    sessionFactory = configuration.buildSessionFactory(serviceRegistry);

    Session sess = sessionFactory.openSession();
    String hql = "SELECT e FROM Event e WHERE e.id = :id";
    Query query = sess.createQuery(hql);
    query.setInteger("id", id);
    List<Event> products;
    List results = query.list();
    products = results;
    
    Users own = null;
    Event venues = null;
    String upto;
    
    Iterator<Event> itr = products.iterator();
    while(itr.hasNext()){
        Event g = itr.next();
        System.out.println(g.getUpto100());
        owner = g.getOwner();
        own = owner;
        venues = g;
    }
    System.out.println(own.getUsername());
    String owns = (String) own.getUsername();
    sess.close();
    if(venues.getUpto300() != null){
        upto = "upto300";
    }else if(venues.getUpto500() != null){
        upto = "upto500";
    }else if(venues.getUpto1000() != null){
        upto = "upto1000";
    }else{
        upto = "upto100";
    }
    session.setAttribute("owner", owner);
    session.setAttribute("venue", venues);
%>
<html>
    <head>
        <style>
            .navy{
                float: right;
            }
        </style>
        <%@include file = "BookHeader.jsp" %>
        <title>Book</title>
    </head>
    <body>
        <div id="lookbookHeader">
        <video autoplay poster="data:image/png;base64, <%=venues.getImage()%>" onerror="this.onerror=null;this.poster='event.png';" id="bgvid">
            
        </video>
        <div id="headerContentContainer">
            <h1 class="whitey"><%=venues.getName()%></h1>
            <div class="initial-arrow-small"></div>
        </div>
        </div>
        </div>
        <div id="introContent">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="https://cdn.jsdelivr.net/momentjs/2.14.1/moment.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
            <br>
            <h2>Book Venue</h2>
            <h5 class="lefty">Email:</h5> <h5 class="rightey"><%=venues.getMail()%></h5><br><br>
            <h5 class="lefty">Max Capacity:</h5> <h5 class="rightey"><%=upto%></h5><br><br>
            <h5 class="lefty">Price for up to 100 people:</h5> <h5 class="rightey"><%=venues.getUpto100()%></h5><br><br>
            <h5 class="lefty">Price for up to 300 people:</h5> <h5 class="rightey"><%=venues.getUpto300()%></h5><br><br>
            <h5 class="lefty">Price for up to 500 people:</h5> <h5 class="rightey"><%=venues.getUpto500() %></h5><br><br>
            <h5 class="lefty">Price for up to 1000 people:</h5> <h5 class="rightey"><%=venues.getUpto1000()%></h5><br><br>
            <h5 class="lefty">Phone no.:</h5> <h5 class="rightey"><%=venues.getPhoneno()%></h5><br><br>
            <h5 class="lefty">Location:</h5> <h5 class="rightey"><%=venues.getLocation()%></h5><br><br>
            <h5 class="lefty">Scenario:</h5> <h5 class="rightey"><%=venues.getScenario()%></h5><br><br>
            <h5 class="lefty">Posted By:</h5> <h5 class="rightey"><%=owns%></h5><br><br>
            <p class="lefty">Description:</p> <p class="rightey">
                <%=venues.getDescription()%>
            </p><br><br>
            <h4>Set Event Details</h4>
            <form action="/Eventer/Booking">
                <!--<script src="//code.jquery.com/jquery-1.10.2.js"></script>
                <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
                <link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css"/>
                <script>
                    $( "#from" ).datepicker().datepicker("setDate", "today");
                    $(function() {  
                        $( "#from" ).datepicker({   
                            defaultDate: "+0w",  
                            changeMonth: true,   
                            numberOfMonths: 1,
                            onClose: function( selectedDate ) {  
                              $( "#to" ).datepicker( "option", "minDate", "today" );
                              $( "#to" ).datepicker( "option", "minDate", selectedDate );
                              $( "#to" ).datepicker( "option", "maxDate", selectedDate);
                            }  
                        });  
                        $( "#to" ).datepicker({
                            defaultDate: "today",
                            changeMonth: true,
                            numberOfMonths: 1,
                            onClose: function( selectedDate ) {
                              $( "#from" ).datepicker( "option", "minDate", "today" );
                            }
                        });  
                    });  
                </script>
                <div class="form-group">
                    <label class="sr-only" for="eventedit">Start date:</label>
                    <input type="text" id="from" name="from" >
                </div>
                <div class="form-group">
                    <label class="sr-only" for="eventedit">End date:</label>
                    <input type="text" id="to" name = "to">
                </div>-->
                <div class="form-group">
                    <label for="Event Name">Event Name:</label>
                    <input type="text" class="form-control" name="eventname" id="" aria-describedby="helpId" placeholder="" required>
                    <small id="emailHelpId" class="form-text text-muted">Enter location of venue</small>
                </div>
                <div class='input-group date' id='datetimepicker1'>
                    <label for="Event Name">Set Event Date:</label>
                    <input name="date" type='text' class="form-control" id="" aria-describedby="helpId" required/><br>
                    <small id="helpId" class="form-text text-muted">Enter date of event</small>
                    <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                    </span><br>
                </div>
                    <script>
                        $(function () {
                            $('#datetimepicker1').datetimepicker();
                            var minDate = $( "#datetimepicker1" ).datepicker( "option", "minDate" );
                            minDate.datepicker( "option", "minDate", "today" );
                        });
                      </script>
                      <h2>Choose Booking Capacity</h2>
                <div class="form-check">
                    <input type="radio" class="form-check-input" name="capacity" id="" checked value="upto100">
                    <label class="form-check-label">
                        <br>   
                        <i class="fa fa-1x" aria-hidden="true"><h3>Up to 100 people</h3></i>
                    </label>
                </div>
                <%
                    if(venues.getUpto300() != null){
                        %>
                        <div class="form-check">
                            <input type="radio" class="form-check-input" name="capacity" id="" value="upto300">
                            <label class="form-check-label">
                                <br>   
                                <i class="fa fa-1x" aria-hidden="true"><h3>Up to 300 people</h3></i>
                            </label>
                        </div>
                        <%
                    }
                    %>
                <%
                    if(venues.getUpto500() != null){
                        %>
                        <div class="form-check">
                            <input type="radio" class="form-check-input" name="capacity" id="" value="upto500">
                            <label class="form-check-label">
                                <br>  
                                <i class="fa fa-1x" aria-hidden="true"><h3>Up to 500 people</h3></i>
                            </label>
                        </div>
                        <%
                    }
                    %>
                <%
                    if(venues.getUpto1000() != null){
                        %>
                        <div class="form-check">
                            <input type="radio" class="form-check-input" name="capacity" id="" value="upto1000">
                            <label class="form-check-label">
                                <br>   
                                <i class="fa fa-1x" aria-hidden="true"><h3>Up to 1000 people</h3></i>
                            </label>
                        </div>
                        <%
                    }
                    %>
                <button class="btn-view">Book Venue</button>
            </form>
        </div>
    </body>
</html>
