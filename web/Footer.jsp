<%-- 
    Document   : Footer
    Created on : Jul 19, 2019, 9:24:55 PM
    Author     : Tariana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<footer class="page-footer foot font-small blue pt-4">
    <div class="container-fluid text-center text-md-left">
        <div class="row">
            <div class="col-md-6 mt-md-0 mt-3">
                <h5 class="text-uppercase">Contact us</h5>
                <p>For more information, here are some of our social media sites to view user feedback as well as direct contacts to our offices</p>
                <p>We are located at:</p>
                <i class="fa fa-map-marker" aria-hidden="true"> Feast Building, 1<sup>st</sup> floor, Room 207
                </i>
            </div>
            <hr class="clearfix w-100 d-md-none pb-3">
            <div class="col-md-3 mb-md-0 mb-3">
                <h5 class="text-uppercase">Social Media</h5>
                <ul class="list-unstyled">
                    <li>
                        <a href="#!"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a>
                    </li>
                    <li>
                        <a href="#!"><i class="fa fa-twitter" aria-hidden="true"></i>Twitter</a>
                    </li>
                    <li>
                        <a href="#!"><i class="fa fa-instagram" aria-hidden="true"></i>Instagram</a>
                    </li>
                    <li>
                        <a href="#!"><i class="fa fa-skype" aria-hidden="true"></i>Skype</a>
                    </li>
                </ul>

            </div>
            <div class="col-md-3 mb-md-0 mb-3">
                <h5 class="text-uppercase">Contacts</h5>
                <ul class="list-unstyled">
                    <li>
                        <a href="#!"><i class="fa fa-envelope" aria-hidden="true"></i>0713456734</a>
                    </li>
                    <li>
                        <a href="#!"><i class="fa fa-whatsapp" aria-hidden="true"></i>0712567899</a>
                    </li>
                    <li>
                        <a href="#!"><i class="fa fa-phone" aria-hidden="true"></i>0711333767</a>
                    </li>
                    <li>
                        <a href="#!"><i class="fa fa-telegram" aria-hidden="true"></i>0712567899</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright text-center py-3">© 2018 Copyright:
        <a href="#">Event Booker</a>
    </div>
</footer>
