<%-- 
    Document   : index
    Created on : Jul 19, 2019, 7:06:41 PM
    Author     : Tariana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="StartHeader.jsp" %>
        <title>Home</title>
    </head>
    <body class="hood">
        <jsp:include page="Home.jsp"></jsp:include>
        <%@include file="Footer.jsp" %>
    </body>
</html>
