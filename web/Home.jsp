<%-- 
    Document   : Home
    Created on : Jul 19, 2019, 9:16:09 PM
    Author     : Tariana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="jumbotron jumbotron-fluid jumbo">
    <div class="container">
        <h1 class="display-3">Event Booker</h1>
        <p class="lead">Book or Advertise your Venues</p>
        <hr class="my-2">
        <p>Book great venues for events ranging from family gatherings, weddings through to graduation ceremonies </p>
        <p class="lead">
            <a class="btn btn-primary btn-lg" href="Choice.jsp" role="button">Proceed...  <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
        </p>
    </div>
</div>
