<%-- 
    Document   : PostEvent
    Created on : Jul 19, 2019, 10:49:34 PM
    Author     : Tariana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file = "AdvertHeader.jsp" %>
        <title>Post Venue</title>
    </head>
    <body class="hood">
        <div class="container forms">
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-lg-12 col-m-12">
                    <h3 class="h3"> POST VENUE </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-lg-12 col-m-12">
                    <form name="eventform" action="/Eventer/PostVenue" onsubmit="return validatesForm()">

                        <img class="image" id="blah" src="fin.png" alt="my image" /><br>
                        <label for="file">Input image of the product:</label><br>
                        <input class="filing" name="image" type='file' onchange="readURL(this);" accept="image/gif, image/jpeg, image/png" required>

                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" name="email" id="" aria-describedby="emailHelpId" placeholder="" required>
                            <small id="emailHelpId" class="form-text text-muted">Enter mail of venue</small>
                        </div>

                        <div class="form-group">
                            <label for="venue">Venue Name:</label>
                            <input type="text" class="form-control" name="venue" id="" aria-describedby="helpId" placeholder="" required>
                            <small id="helpId" class="form-text text-muted">Enter venue name with minimum of 5 characters</small>
                        </div>

                        <div class="form-group">
                            <label for="upto">Select the capacity:</label>
                            <select class="form-control" name="upto" id="upto" onchange="selection()">
                                <option value="upto100">up to 100 people</option>
                                <option value="upto300">up to 300 people</option>
                                <option value="upto500">up to 500 people</option>
                                <option value="upto1000">up to 1000 people</option>
                            </select>
                            <small id="helpId" class="form-text text-muted">Enter capacity to input pricing for different capacity</small>
                        </div>

                        <div class="form-group">
                            <label for="upto100">Pricing for 100:</label>
                            <input type="number" min="0" class="form-control" name="upto100" id="" aria-describedby="helpId" placeholder="" required>
                            <small id="helpId" class="form-text text-muted">Enter Pricing for up to 100 people</small>
                        </div>

                        <div class="form-group threehundred" id="threehundred">
                            <label for="upto300">Pricing for 300:</label>
                            <input type="number" min="0" class="form-control" name="upto300" id="threehund" aria-describedby="helpId" placeholder="">
                            <small id="helpId" class="form-text text-muted">Enter Pricing for up to 300 people</small>
                        </div>

                        <div class="form-group fivehundred" id="fivehundred">
                            <label for="upto500">Pricing for 500:</label>
                            <input type="number" min="0" class="form-control" name="upto500" id="fivehund" aria-describedby="helpId" placeholder="">
                            <small id="helpId" class="form-text text-muted">Enter Pricing for up to 500 people</small>
                        </div>

                        <div class="form-group thousand" id="thousand">
                            <label for="upto1000">Pricing for 1000:</label>
                            <input type="number" min="0" class="form-control" name="upto1000" id="onethou" aria-describedby="helpId" placeholder="">
                            <small id="helpId" class="form-text text-muted">Enter Pricing for up to 1000 people</small>
                        </div>

                        <div class="form-group">
                            <label for="phoneno">Phone No.:</label>
                            <input type="number" min="0" name="phoneno" id="" class="form-control" placeholder="" required aria-describedby="helpId">
                            <small id="helpId" class="text-muted">Enter phone number all 9 characters</small>
                        </div>

                        <div class="form-group">
                            <label for="location">Location:</label>
                            <input type="text" class="form-control" name="location" id="" aria-describedby="helpId" placeholder="" required>
                            <small id="emailHelpId" class="form-text text-muted">Enter location of venue</small>
                        </div>
                        
                        <label class="form-check-label">
                          Scenario:
                        </label>
                        <br>

                        <div class="form-check">
                            <label for="Scenario" class="form-check-label">
                              <input type="radio" class="form-check-input" name="Scenario" id="" value="Indoor" checked>
                              <i class="fa fa-home fa-2x" aria-hidden="true"></i><i class="fa fa-1x" aria-hidden="true">Indoor</i>
                            </label>
                        </div>

                        <div class="form-check">
                            <label for="Scenario" class="form-check-label">
                              <input type="radio" class="form-check-input" name="Scenario" id="" value="Outdoor">
                              <i class="fa fa-tree fa-2x" aria-hidden="true"></i><i class="fa fa-1x" aria-hidden="true">Outdoor</i>
                            </label>
                        </div>
                        <br>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" id="" placeholder="Enter venue description"></textarea>
                        </div>

                        <div class="sign">
                            <button type="submit" class="btn btn-primary">Post Venue</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
