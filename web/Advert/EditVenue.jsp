<%-- 
    Document   : EditEvent
    Created on : Jul 19, 2019, 10:49:53 PM
    Author     : Tariana
--%>

<%@page import="java.util.Iterator"%>
<%@page import="entities.Event"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.service.ServiceRegistryBuilder"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="org.hibernate.service.ServiceRegistry"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page import="entities.Users"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String eventid = (String) session.getAttribute("venueid");
    Users owner;
    int id = Integer.parseInt(eventid);
    
    SessionFactory sessionFactory;
    ServiceRegistry serviceRegistry;

    Configuration configuration = new Configuration();
    configuration.addAnnotatedClass(Users.class)
        .configure();

    serviceRegistry = new ServiceRegistryBuilder()
            .applySettings(configuration.getProperties())
            .configure("hibernate.cfg.xml")
            .build();        
    sessionFactory = configuration.buildSessionFactory(serviceRegistry);

    Session sess = sessionFactory.openSession();
    String hql = "SELECT e FROM Event e WHERE e.id = :id";
    Query query = sess.createQuery(hql);
    query.setInteger("id", id);
    List<Event> products;
    List results = query.list();
    products = results;
    
    Users own = null;
    Event venues = null;
    String upto = "upto100";
    
    Iterator<Event> itr = products.iterator();
    while(itr.hasNext()){
        Event g = itr.next();
        System.out.println(g.getUpto100());
        owner = g.getOwner();
        own = owner;
        venues = g;
    }
    session.setAttribute("venue", venues);
    System.out.println(own.getUsername());
    String owns = (String) own.getUsername();
    sess.close();
    if(venues.getUpto1000() != null){
        upto = "upto1000";
    }else if(venues.getUpto500() != null){
        upto = "upto500";
    }else if(venues.getUpto300() != null){
        upto = "upto300";
    } 
    System.out.println(upto);
%>
<html>
    <head>
        <%@include file = "AdvertHeader.jsp" %>
        <title>Edit Venue</title>
    </head>
    <body class="hood">
        <div class="container forms">
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-lg-12 col-m-12">
                    <h3 class="h3"> EDIT VENUE </h3>
                    <button class="btn-write" onclick="activate()" id="activator"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button class="btn-write" onclick="deactivate()" id="deactivator" hidden><i class="fa fa-close" aria-hidden="true"></i></button>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-lg-12 col-m-12">
                    <form class="changeable" id="formID" name="eventform" action="/Eventer/EditVenue" onsubmit="return validatesForm()">

                        <img class="image" id="blah" src="data:image/png;base64, <%=venues.getImage()%>" onerror="this.onerror=null;this.src='event.png';" alt="my image" /><br>
                        <label for="file">Input image of the product:</label><br>
                        <input class="filing" name="image" type='file' onchange="readURL(this);" accept="image/gif, image/jpeg, image/png" disabled>

                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" name="email" value="<%=venues.getMail()%>" id="" aria-describedby="emailHelpId" required placeholder="" disabled>
                            <small id="emailHelpId" class="form-text text-muted">Enter mail of venue</small>
                        </div>

                        <div class="form-group">
                            <label for="venue">Venue Name:</label>
                            <input type="text" class="form-control" name="venue" value="<%=venues.getName()%>" id="" aria-describedby="helpId" required placeholder="" disabled>
                            <small id="helpId" class="form-text text-muted">Enter venue name with minimum of 5 characters</small>
                        </div>

                        <div class="form-group">
                            <label for="upto">Select the capacity:</label>
                            <select class="form-control" name="upto" id="upto" onchange="selection()" value="" disabled>
                                <option value="upto100">up to 100 people</option>
                                <option value="upto300">up to 300 people</option>
                                <option value="upto500">up to 500 people</option>
                                <option value="upto1000">up to 1000 people</option>
                                </select>
                                    
                            <small id="helpId" class="form-text text-muted">Enter capacity to input pricing for different capacity</small>
                        </div>

                        <div class="form-group">
                            <label for="upto100">Pricing for 100:</label>
                            <input type="number" min="0" class="form-control" name="upto100" value="<%=venues.getUpto100()%>" id="" aria-describedby="helpId" placeholder="" required disabled>
                            <small id="helpId" class="form-text text-muted">Enter Pricing for up to 100 people</small>
                        </div>
                            
                        <!--upto 300-->    
                        <%
                            if(upto == "upto100"){
                                %>
                            <div class="form-group threehundred" id="threehundred">
                                <label for="upto300">Pricing for 300:</label>
                                <input type="number" min="0" class="form-control" name="upto300" value="" id="threehund" aria-describedby="helpId" placeholder="" disabled>
                                <small id="helpId" class="form-text text-muted">Enter Pricing for up to 300 people</small>
                            </div>
                                <%
                            }else{
                            %>
                            <div class="form-group threehundred" id="threehundred">
                                <label for="upto300">Pricing for 300:</label>
                                <input type="number" min="0" class="form-control" name="upto300" value="<%=venues.getUpto300()%>" id="threehund" aria-describedby="helpId" placeholder="" disabled>
                                <small id="helpId" class="form-text text-muted">Enter Pricing for up to 300 people</small>
                            </div>
                            <script>
                                var e = document.getElementById("upto");
                                var opt = e.options[e.selectedIndex].value;
                                console.log(opt);
                                console.log(e.selectedIndex);
                                var x = document.getElementById("threehund");
                                var x0 = document.getElementById("fivehund");
                                var x1 = document.getElementById("onethou");
                                x.setAttribute("required", "");
                                document.getElementById("threehundred").style.display = "block";
                                console.log(x);
                                document.getElementById('upto').getElementsByTagName('option')[1].selected = true;
                            </script>
                                <%
                            }
                        %>
                        
                        <!--upto 500-->
                        <%
                            if(upto == "upto100" || upto == "upto300"){
                                %>
                                <div class="form-group fivehundred" id="fivehundred">
                                    <label for="upto500">Pricing for 500:</label>
                                    <input type="number" min="0" class="form-control" name="upto500" value="" id="fivehund" aria-describedby="helpId" placeholder="" disabled>
                                    <small id="helpId" class="form-text text-muted">Enter Pricing for up to 500 people</small>
                                </div>
                                <%
                            }else{
                                %>
                                <div class="form-group fivehundred" id="fivehundred">
                                    <label for="upto500">Pricing for 500:</label>
                                    <input type="number" min="0" class="form-control" name="upto500" value="<%=venues.getUpto500()%>" id="fivehund" aria-describedby="helpId" placeholder="" disabled>
                                    <small id="helpId" class="form-text text-muted">Enter Pricing for up to 500 people</small>
                                </div>
                                <script>
                                    var e = document.getElementById("upto");
                                    var opt = e.options[e.selectedIndex].value;
                                    console.log(opt);
                                    console.log(e.selectedIndex);
                                    var x = document.getElementById("threehund");
                                    var x0 = document.getElementById("fivehund");
                                    var x1 = document.getElementById("onethou");
                                    x.setAttribute("required", "");
                                    x0.setAttribute("required", "");
                                    document.getElementById("fivehundred").style.display = "block";
                                    console.log(x);
                                    document.getElementById('upto').getElementsByTagName('option')[2].selected = true;
                                </script>
                                <%
                            }
                        %>
                        
                        <%
                            if(upto == "upto1000"){
                                %>
                                <div class="form-group thousand" id="thousand">
                                    <label for="upto1000">Pricing for 1000:</label>
                                    <input type="number" min="0" class="form-control" name="upto1000" value="<%=venues.getUpto1000()%>" id="onethou" aria-describedby="helpId" placeholder="" disabled>
                                    <small id="helpId" class="form-text text-muted">Enter Pricing for up to 1000 people</small>
                                </div>
                                <script>
                                    var e = document.getElementById("upto");
                                    var opt = e.options[e.selectedIndex].value;
                                    console.log(opt);
                                    console.log(e.selectedIndex);
                                    var x = document.getElementById("threehund");
                                    var x0 = document.getElementById("fivehund");
                                    var x1 = document.getElementById("onethou");
                                    x.setAttribute("required", "");
                                    x0.setAttribute("required", "");
                                    x1.setAttribute("required", "");
                                    document.getElementById("thousand").style.display = "block";
                                    document.getElementById('upto').getElementsByTagName('option')[3].selected = true;
                                </script>
                                <%
                            }else{
                                %>
                                <div class="form-group thousand" id="thousand">
                                    <label for="upto1000">Pricing for 1000:</label>
                                    <input type="number" min="0" class="form-control" name="upto1000" value="" id="onethou" aria-describedby="helpId" placeholder="" disabled>
                                    <small id="helpId" class="form-text text-muted">Enter Pricing for up to 1000 people</small>
                                </div>
                                <%
                            }
                        %>
                        

                        <div class="form-group">
                            <label for="phoneno">Phone No.:</label>
                            <input type="number" min="0" name="phoneno" id="" class="form-control" value="<%=venues.getPhoneno()%>" placeholder="" required aria-describedby="helpId" disabled>
                            <small id="helpId" class="text-muted">Enter phone number all 9 characters</small>
                        </div>
                        
                        <div class="form-group">
                            <label for="location">Location:</label>
                            <input type="text" class="form-control" name="location" id="" value="<%=venues.getLocation()%>" aria-describedby="helpId" placeholder="" required disabled>
                            <small id="emailHelpId" class="form-text text-muted">Enter location of venue</small>
                        </div>

                        <label class="form-check-label">
                              Scenario:
                            </label>
                        <br>
                        <%
                            if(venues.getScenario().equals("Indoor")){
                                %>
                                <div class="form-check">
                                    <label class="form-check-label">
                                          <input type="radio" class="form-check-input" name="Scenario" id="" value="Indoor" checked disabled>
                                          <i class="fa fa-home fa-2x" aria-hidden="true"></i><i class="fa fa-1x" aria-hidden="true">Indoor</i>
                                        </label>
                                </div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                          <input type="radio" class="form-check-input" name="Scenario" id="" value="Outdoor" disabled>
                                          <i class="fa fa-tree fa-2x" aria-hidden="true"></i><i class="fa fa-1x" aria-hidden="true">Outdoor</i>
                                        </label>
                                </div>
                                <%
                            }else if(venues.getScenario().equals("Outdoor")){
                                %>
                                <div class="form-check">
                                    <label class="form-check-label">
                                          <input type="radio" class="form-check-input" name="Scenario" id="" value="Indoor" disabled>
                                          <i class="fa fa-home fa-2x" aria-hidden="true"></i><i class="fa fa-1x" aria-hidden="true">Indoor</i>
                                    </label>
                                </div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                          <input type="radio" class="form-check-input" name="Scenario" id="" value="Outdoor" checked disabled>
                                          <i class="fa fa-tree fa-2x" aria-hidden="true"></i><i class="fa fa-1x" aria-hidden="true">Outdoor</i>
                                    </label>
                                </div>
                                <%
                            }
                        %>
                        
                        <br>
                        
                        <%
                            if(venues.getDescription() == null){
                                %>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea class="form-control" name="description" id=""  placeholder="" value="" disabled></textarea>
                                </div>
                                <%
                            }else{
                                %>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea class="form-control" name="description" id=""  placeholder="" value="" disabled><%=venues.getDescription().toString()%>
                                    </textarea>
                                </div>
                                <%
                            }
                        %>
                        

                        <div class="sign">
                            <button type="submit" class="btn btn-primary" disabled>Edit Venue</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
