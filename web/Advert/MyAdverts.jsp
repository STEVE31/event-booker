<%-- 
    Document   : MyAdverts
    Created on : Jul 19, 2019, 10:25:32 PM
    Author     : Tariana
--%>

<%@page import="java.util.Iterator"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.service.ServiceRegistryBuilder"%>
<%@page import="entities.*"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="org.hibernate.service.ServiceRegistry"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    SessionFactory sessionFactory;
    ServiceRegistry serviceRegistry;

    Configuration configuration = new Configuration();
    configuration.addAnnotatedClass(Users.class)
        .configure();

    serviceRegistry = new ServiceRegistryBuilder()
            .applySettings(configuration.getProperties())
            .configure("hibernate.cfg.xml")
            .build();        
    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    
    Users owner;
    Session sess = sessionFactory.openSession();
    String hql = "FROM Event";
    Query queryy = sess.createQuery(hql);
    List<Event> buying;
    List results = queryy.list();
    buying = results;
    Iterator<Event> itr = buying.iterator();
    int i = 0;
    
    %>
<html>
    <head>
        <%@include file = "AdvertHeader.jsp" %>
        <title><%=session.getAttribute("uname")%> venues</title>
    </head>
    <body class="eventer">
        <div class="kadi">
            <%  
                int u = 0;
                while(itr.hasNext()){
                    Event g = itr.next();
                    System.out.println(g.getUpto100());
                    owner = g.getOwner();
                    String mail = (String) session.getAttribute("mail");
                    
                    if(owner.getEmail().equalsIgnoreCase(mail)){
                        u++;
                        %>
                    <div class="card">
                        <img class="card-img-top" src="data:image/png;base64, <%=g.getImage()%>" onerror="this.onerror=null;this.src='event.png';"  alt="">
                        <div class="card-body">
                            <h4 class="card-title"><%=g.getName()%></h4>
                            <p class="card-text"><%=g.getLocation()%></p>
                            <p class="card-text eve">Owned by: </p><b><%=owner.getUsername()%></b>
                            <form action="/Eventer/Redirect">
                                <div class="form-group">
                                    <label class="sr-only" for="eventdelete">Hidden input label</label>
                                    <input type="text" class="form-control" name="redirect" id="inputName" placeholder="" value="deletevenue" hidden>
                                    <input type="text" class="form-control" name="eventid" id="inputName" placeholder="" value="<%=g.getId()%>" hidden>
                                    <button type="submit" class="btn-delete btn-danger ">Delete</button>
                                </div>
                            </form>
                            <form action="/Eventer/Redirect">
                                <div class="form-group">
                                    <label class="sr-only" for="eventedit">Hidden input label</label>
                                    <input type="text" class="form-control" name="redirect" id="inputName" placeholder="" value="editvenue" hidden>
                                    <input type="text" class="form-control" name="eventid" id="inputName" placeholder="" value="<%=g.getId()%>" hidden>
                                    <button type="submit" class="btn-edit btn-success btn-view">Edit</button>
                                </div>
                            </form>
                            <form action="/Eventer/Redirect">
                                <div class="form-group">
                                    <label class="sr-only" for="eventview">Hidden input label</label>
                                    <input type="text" class="form-control" name="redirect" id="inputName" placeholder="" value="userviewvenue" hidden>
                                    <input type="text" class="form-control" name="eventid" id="inputName" placeholder="" value="<%=g.getId()%>" hidden>
                                    <button type="submit" class="btn-view">View Venue</button>
                                </div>
                            </form>
                        </div>
                    </div>          
            <%
                    }  
                }
                if(u == 0){
                        %>
                        <h1 style="align-content: center;">You have no available venues</h1>
                        <%
                    }
                sess.close();
                %>
        </div>
    </body>
</html>
