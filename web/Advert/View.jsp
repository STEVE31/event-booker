<%-- 
    Document   : View
    Created on : Jul 21, 2019, 8:32:48 PM
    Author     : Tariana
--%>

<%@page import="java.util.Iterator"%>
<%@page import="entities.Event"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.service.ServiceRegistryBuilder"%>
<%@page import="entities.Users"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="org.hibernate.service.ServiceRegistry"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String eventid = (String) session.getAttribute("venueid");
    Users owner;
    int id = Integer.parseInt(eventid);
    
    SessionFactory sessionFactory;
    ServiceRegistry serviceRegistry;

    Configuration configuration = new Configuration();
    configuration.addAnnotatedClass(Users.class)
        .configure();

    serviceRegistry = new ServiceRegistryBuilder()
            .applySettings(configuration.getProperties())
            .configure("hibernate.cfg.xml")
            .build();        
    sessionFactory = configuration.buildSessionFactory(serviceRegistry);

    Session sess = sessionFactory.openSession();
    String hql = "SELECT e FROM Event e WHERE e.id = :id";
    Query query = sess.createQuery(hql);
    query.setInteger("id", id);
    List<Event> products;
    List results = query.list();
    products = results;
    
    Users own = null;
    Event venues = null;
    String upto;
    
    Iterator<Event> itr = products.iterator();
    while(itr.hasNext()){
        Event g = itr.next();
        System.out.println(g.getUpto100());
        owner = g.getOwner();
        own = owner;
        venues = g;
    }
    System.out.println(own.getUsername());
    String owns = (String) own.getUsername();
    sess.close();
    if(venues.getUpto300() != null){
        upto = "upto300";
    }else if(venues.getUpto500() != null){
        upto = "upto500";
    }else if(venues.getUpto1000() != null){
        upto = "upto1000";
    }else{
        upto = "upto100";
    }
%>
<html>
    <head>
        <%@include file = "AdvertHeader.jsp" %>
        <title>View</title>
    </head>
    <body>
        <div id="lookbookHeader">
        <video autoplay poster="data:image/png;base64, <%=venues.getImage()%>" onerror="this.onerror=null;this.poster='event.png';" id="bgvid">
            
        </video>
        <div id="headerContentContainer">
            <h1 class="whitey"><%=venues.getName()%></h1>
            <div class="initial-arrow-small"></div>
        </div>
        </div>
        </div>
        <div id="introContent">
            <br>
            <h5 class="lefty">Email:</h5> <h5 class="rightey"><%=venues.getMail()%></h5><br><br>
            <h5 class="lefty">Max Capacity:</h5> <h5 class="rightey"><%=upto%></h5><br><br>
            <h5 class="lefty">Price for up to 100 people:</h5> <h5 class="rightey"><%=venues.getUpto100()%></h5><br><br>
            <h5 class="lefty">Price for up to 300 people:</h5> <h5 class="rightey"><%=venues.getUpto300()%></h5><br><br>
            <h5 class="lefty">Price for up to 500 people:</h5> <h5 class="rightey"><%=venues.getUpto500() %></h5><br><br>
            <h5 class="lefty">Price for up to 1000 people:</h5> <h5 class="rightey"><%=venues.getUpto1000()%></h5><br><br>
            <h5 class="lefty">Phone no.:</h5> <h5 class="rightey"><%=venues.getPhoneno()%></h5><br><br>
            <h5 class="lefty">Location:</h5> <h5 class="rightey"><%=venues.getLocation()%></h5><br><br>
            <h5 class="lefty">Scenario:</h5> <h5 class="rightey"><%=venues.getScenario()%></h5><br><br>
            <h5 class="lefty">Posted By:</h5> <h5 class="rightey"><%=owns%></h5><br><br>
            <p class="lefty">Description:</p> <p class="rightey">
                <%=venues.getDescription()%>
            </p><br><br>
            <a href="/Eventer/Advert/MyAdverts.jsp"><button class="btn-view">Back</button></a>
        </div>
    </body>
</html>
