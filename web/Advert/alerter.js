function activate() {
    var inputs = document.getElementsByTagName('input');
    for (var i = 0; i < inputs.length; i++) {
        inputs[i].disabled = false;
    }

    var buttons = document.getElementsByTagName('button');
    for (var i = 0; i < buttons.length; i++) {
        buttons[i].disabled = false;
    }

    var d = document.getElementById('formID');
    d.classList.remove('changeable');
    var g = document.getElementById('activator');
    g.hidden = true;
    g.disabled = true;
    var h = document.getElementById('deactivator');
    h.hidden = false;
    var areas = document.getElementsByTagName('textarea');
    for (var i = 0; i < areas.length; i++) {
        areas[i].disabled = false;
    }
    var selects = document.getElementsByTagName('select');
    for (var i = 0; i < selects.length; i++) {
        selects[i].disabled = false;
    }
    alert("form can now be edited. click okay to continue");
}

function deactivate() {
    var inputs = document.getElementsByTagName('input');
    for (var i = 0; i < inputs.length; i++) {
        inputs[i].disabled = true;
    }

    var buttons = document.getElementsByTagName('button');
    for (var i = 0; i < buttons.length; i++) {
        buttons[i].disabled = true;
    }

    var d = document.getElementById('formID');
    d.classList.add('changeable');
    var g = document.getElementById('activator');
    g.disabled = false;
    g.hidden = false;
    var h = document.getElementById('deactivator');
    h.hidden = true;
    var areas = document.getElementsByTagName('textarea');
    for (var i = 0; i < areas.length; i++) {
        areas[i].disabled = true;
    }
    var selects = document.getElementsByTagName('select');
    for (var i = 0; i < selects.length; i++) {
        selects[i].disabled = true;
    }
    alert("form editing is closed. click okay to continue");
}