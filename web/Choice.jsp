<%-- 
    Document   : Choice
    Created on : Jul 19, 2019, 9:16:47 PM
    Author     : Tariana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file = "ChoiceHeader.jsp" %>
        <title>Choice</title>
    </head>
    <body class="hood">
        <div class="container-fluid">
            <div class="row new">
                <div class="col-xs-6 col-sm-12 col-md-12 col-lg-12 col-xl-6">
                    <a class="nochanges" href="Book/PickVenue.jsp">
                        <div class="ne">
                            <h2> Book Area </h2>
                            <div class="newer">
                                <i class="fa fa-5x fa-calendar" aria-hidden="true"></i>
                                <i class="fa fa-5x fa-calendar-check-o" aria-hidden="true"></i>
                                <i class="fa fa-5x fa-calendar-minus-o" aria-hidden="true"></i>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-12 col-md-12 col-lg-12 col-xl-6">
                    <a class="nochanges" href="Advert/MyAdverts.jsp">
                        <div class="ne">
                            <h2> Advertise </h2>
                            <div class="newer">
                                <i class="fa fa-5x fa-birthday-cake" aria-hidden="true"></i>
                                <i class="fa fa-5x fa-coffee" aria-hidden="true"></i>
                                <i class="fa fa-5x fa-spoon" aria-hidden="true"></i><br>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <%@include file="Footer.jsp" %>
    </body>
</html>
