<%-- 
    Document   : Header
    Created on : Jul 19, 2019, 11:01:34 PM
    Author     : Tariana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" media="screen" href="main.css" />
<script src="main.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!--<nav class="navbar navbar-inverse fixed-top">
  <div class="container-fixed">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="../index.jsp">Eventer</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="../index.jsp">Home</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="../Choice.jsp"><span class="glyphicon glyphicon-user"></span>Choice</a></li>
        <li><a href="SignUp.jsp"><span class="glyphicon glyphicon-user"></span>SignUp</a></li>
        <li><a href="Login.jsp"><span class="glyphicon glyphicon-user"></span>Login</a></li>
        </li>
      </ul>
    </div>
  </div>
</nav>
<style>
    nav{
        position: absolute;
    }
</style>-->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/Eventer/index.jsp">Eventer</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="../index.jsp">
            <i class="fa fa-1x fa-home" aria-hidden="true">
            </i>
            Home <span class="sr-only">(current)</span>
        </a>
      </li>
      <li>
        <a class="nav-link" href="../Choice.jsp">
            <i class="fa fa-1x fa-pencil" aria-hidden="true">
            </i>
            Choice
        </a>
      </li>
    </ul>
    <span class="navbar-text">
        <a class="nav-link" href="Login.jsp">
            <i class="fa fa-1x fa-sign-in" aria-hidden="true">
            </i>
            Login
        </a>
    </span>
    <span class="navbar-text">
        <a class="nav-link" href="SignUp.jsp">
            <i class="fa fa-1x fa-user" aria-hidden="true">
            </i>
            SignUp
        </a>
    </span>
  </div>
</nav> 
