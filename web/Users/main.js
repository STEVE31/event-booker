/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#blah')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function validateForm() {
    var fname = document.forms["regform"]["fname"];
    var lname = document.forms["regform"]["lname"];
    var password = document.forms["regform"]["pwd"];
    var username = document.forms["regform"]["username"];
    var phoneno = document.forms["regform"]["phoneno"];
    if (username.value.length < 5) {
        alert("Username should atleast be 5 characters");
        return false;
    }else if (password.value.length < 5) {
        alert("Password should atleast be 5 characters");
        return false;
    } else if (fname.value.length < 2) {
        alert("First name should atleast be 2 characters");
        return false;
    }else if (lname.value.length < 4) {
        alert("Last name should atleast be 4 characters");
        return false;
    }else if (phoneno.value.length != 9) {
        alert("Phone number contains 9 characters");
        return false;
    }else {
        return true;
    }
}

function validatesForm() {
    var password = document.forms["logform"]["pwd"];
    if (password.value.length < 5) {
        alert("Password should atleast be 5 characters");
        return false;
    }else {
        return true;
    }
} 


