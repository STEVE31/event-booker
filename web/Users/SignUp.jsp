<%-- 
    Document   : SignUp
    Created on : Jul 19, 2019, 10:58:48 PM
    Author     : Tariana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if(session.getAttribute("mail") != null){
        response.sendRedirect("/Eventer/Choice.jsp");
    }
%>
<html>
    <head>
        <%@include file = "UserHeader.jsp" %>
        <title>Sign Up</title>
    </head>
    <body class="hood">
        <div class="container forms">
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-lg-12 col-m-12">
                    <h3 class="h3"> SIGN UP </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-lg-12 col-m-12">
                    <form name="regform" action="/Eventer/Register" onsubmit="return validateForm()">

                        <img class="img" id="blah" src="profile.png" alt="my image" /><br>
                        <label for="file">Add profile picture:</label><br>
                        <input class="filing" name="image" type='file' onchange="readURL(this);" accept="image/gif, image/jpeg, image/png" required>

                        <div class="form-group">
                            <label for="">Email:</label>
                            <input type="email" class="form-control" name="email" id="" aria-describedby="emailHelpId" placeholder="" required>
                            <small id="emailHelpId" class="form-text text-muted">Enter email (someone@example.com)</small>
                        </div>

                        <div class="form-group">
                            <label for="">Username:</label>
                            <input type="text" class="form-control" name="username" id="" aria-describedby="helpId" placeholder="" min="5" required>
                            <small id="helpId" class="form-text text-muted">Enter Username with minimum of 5 characters</small>
                        </div>

                        <div class="form-group">
                            <label for="">Password:</label>
                            <input type="password" class="form-control" name="pwd" id="" placeholder="" required>
                        </div>

                        <div class="form-group">
                            <label for="">First Name:</label>
                            <input type="text" class="form-control" name="fname" id="" aria-describedby="helpId" placeholder="" required>
                            <small id="helpId" class="form-text text-muted">Enter First Name</small>
                        </div>

                        <div class="form-group">
                            <label for="">Last Name:</label>
                            <input type="text" class="form-control" name="lname" id="lname" aria-describedby="helpId" placeholder="" required>
                            <small id="helpId" class="form-text text-muted">Enter Last Name</small>
                        </div>

                        <div class="form-group">
                            <label for="">Phone No.:</label>
                            <input type="number" min="0" name="phoneno" id="" class="form-control" required placeholder="" aria-describedby="helpId">
                            <small id="helpId" class="text-muted">Enter phone number all 9 characters</small>
                        </div>


                        <label class="form-check-label">
                          Gender:
                        </label>
                        <br>

                        <div class="form-check">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="Sex" id="" value="Male">
                              <i class="fa fa-male" aria-hidden="true">Male</i>
                            </label>
                        </div>

                        <div class="form-check">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="Sex" id="" value="Female" checked>
                              <i class="fa fa-female" aria-hidden="true">Female</i>
                            </label>
                        </div>
                        <div>
                            <p>Already have an account?
                                <a href="Login.jsp">Login</a>
                            </p>
                        </div>
                        <div class="sign">
                            <button type="submit" class="btn btn-primary">Register</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
