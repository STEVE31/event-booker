<%-- 
    Document   : Login
    Created on : Jul 19, 2019, 10:58:23 PM
    Author     : Tariana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if(session.getAttribute("mail") != null){
        response.sendRedirect("/Eventer/Choice.jsp");
    }
%>
<html>
    <head>
        <%@include file = "UserHeader.jsp" %>
        <title>Login</title>
    </head>
    <body class="hood">
        <div class="container forms login">
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-lg-12 col-m-12">
                    <h3 class="h3">LOGIN</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-lg-12 col-m-12">
                    <form name="logform" action="/Eventer/Login" onsubmit="return validatesForm()">
                        <div class="form-group">
                            <label for="">Email:</label>
                            <input type="email" class="form-control" name="email" id="" aria-describedby="emailHelpId" placeholder="" required>
                            <small id="emailHelpId" class="form-text text-muted">Input Email (someone@example.com)</small>
                        </div>

                        <div class="form-group">
                            <label for="">Password:</label>
                            <input type="password" class="form-control" name="pwd" id="" placeholder="" required>
                        </div>
                        <div>
                            <p>Do not have account?
                                <a href="SignUp.jsp">SignUp</a>
                            </p>
                        </div>
                        <button type="submit" class="btn btn-primary">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
